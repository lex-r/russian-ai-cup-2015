import model.Car;
import model.Game;
import model.Move;
import model.World;

import static java.lang.StrictMath.*;

public final class MyStrategy implements Strategy {

    private static double nextWaypointX = 0;
    private static double nextWaypointY = 0;

    // выполняется ли сейчас разворот
    private static boolean isUTurn = false;

    private static double[] speeds = new double[]{2,3,4,5,6};

    @Override
    public void move(Car self, World world, Game game, Move move) {

        double nextWaypointXDef = self.getNextWaypointX();
        double nextWaypointYDef = self.getNextWaypointY();

        if (MyStrategy.nextWaypointX != nextWaypointXDef || MyStrategy.nextWaypointY != nextWaypointYDef) {
            MyStrategy.nextWaypointX = nextWaypointXDef;
            MyStrategy.nextWaypointY = nextWaypointYDef;

            System.out.println("self.getNextWaypointX: " + self.getNextWaypointX());
            System.out.println("self.getNextWaypointY: " + self.getNextWaypointY());
            System.out.println();
        }

        double nextWaypointX = (self.getNextWaypointX() + 0.5D) * game.getTrackTileSize();
        double nextWaypointY = (self.getNextWaypointY() + 0.5D) * game.getTrackTileSize();

        System.out.println("nextWaypointX: " + nextWaypointX);
        System.out.println("nextWaypointY: " + nextWaypointY);

        System.out.println("XSpeed: " + self.getSpeedX());
        System.out.println("YSpeed: " + self.getSpeedY());

        double angleToNext = self.getAngleTo(nextWaypointX, nextWaypointY);

        System.out.println("NextXY: " + nextWaypointX + " " + nextWaypointY);
        System.out.println("MyXY: " + self.getX() + " " + self.getY());
        System.out.println("MyAngle: " + self.getAngle());
        System.out.println("AngleToNext: " + angleToNext);

        System.out.println("Tick: " + world.getTick());

        System.out.println();

        double cornerTileOffset = 0.3D * game.getTrackTileSize();

        boolean isCorner = false;
        boolean toBreak = false;

        switch (world.getTilesXY()[self.getNextWaypointX()][self.getNextWaypointY()]) {
            case LEFT_TOP_CORNER:
                nextWaypointX += cornerTileOffset;
                nextWaypointY += cornerTileOffset;
                isCorner = true;
                break;
            case RIGHT_TOP_CORNER:
                nextWaypointX -= cornerTileOffset;
                nextWaypointY += cornerTileOffset;
                isCorner = true;
                break;
            case LEFT_BOTTOM_CORNER:
                nextWaypointX += cornerTileOffset;
                nextWaypointY -= cornerTileOffset;
                isCorner = true;
                break;
            case RIGHT_BOTTOM_CORNER:
                nextWaypointX -= cornerTileOffset;
                nextWaypointY -= cornerTileOffset;
                isCorner = true;
                break;
            default:
        }

        double angleToWaypoint = self.getAngleTo(nextWaypointX, nextWaypointY);
        double speedModule = hypot(self.getSpeedX(), self.getSpeedY());

        // если старт дан, то считаем показатели скорости
        if (world.getTick() > game.getInitialFreezeDurationTicks() && world.getTick() % 50 == 0) {
            pushSpeed(self);
            System.out.print("Speeds: ");
            for (double s: speeds) {
                System.out.print(s + " ");
            }
            System.out.println();
        }

        if (abs(angleToWaypoint) > 2D * PI / 8 && !isUTurn && isStopped()) {
            isUTurn = true;
        }

        if (isUTurn) {
            move.setEnginePower(-1D);
            if (angleToWaypoint > 0) {
                move.setWheelTurn(-1D);
            } else {
                move.setWheelTurn(1D);
            }
            if (abs(angleToWaypoint) < 2D * PI / 8) {
                move.setBrake(true);
                isUTurn = false;
            }
        } else {
            move.setWheelTurn(angleToWaypoint * 32.0D / PI);
            move.setEnginePower(1D);
        }

        if (speedModule * speedModule * abs(angleToWaypoint) > 2.5D * 2.5D * PI) {
            move.setBrake(true);
        }

        // если можно разлить мазуту
        if (move.isSpillOil()) {
            move.setSpillOil(true);
        }

        move.setThrowProjectile(true);
    }

    protected void pushSpeed(Car car) {
        for (int i = 1; i < speeds.length; i++) {
            speeds[i - 1] = speeds[i];
        }
        speeds[4] = car.getSpeedX() + car.getSpeedY();
    }

    protected boolean isStopped() {
        double average = 0;
        for (int i = 0; i < speeds.length; i++) {
            average += abs(speeds[i]);
        }

        average /= speeds.length;

        return average < 1;
    }
}